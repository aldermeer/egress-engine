package com.dysarcana.egressengine.components;

import com.artemis.Component;

public class Combat extends Component {
	
	public enum AttackLength { Tiny, Short, Medium, Long }
	public enum AttackType { Piercing, Blunt, Sharp, Constriction }
	

	public int hp;
	public int stamina;
	
	public AttackLength attackLength;
	public AttackType attackType;
	
	
}
