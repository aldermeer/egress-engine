package com.dysarcana.egressengine.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;

/**
 * Targeting component
 * @author J Michael Welch
 *
 */
public class FocalPoint extends Component {
	public FocalPoint(float x, float y) {
		vec.x = x;
		vec.y = y;
	}

	public Vector2 vec = new Vector2();
	
}
