package com.dysarcana.egressengine.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;

public class Location extends Component {
	public Vector2 vec = new Vector2();
	
	public Location(float x,float y) {
		vec.x = x;
		vec.y = y;
	}
}
