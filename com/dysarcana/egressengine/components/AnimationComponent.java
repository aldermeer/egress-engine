package com.dysarcana.egressengine.components;

import com.artemis.Component;

public class AnimationComponent extends Component {

	public static enum AnimationState {
		Idle, Exhausted, Sneaking, Walking, Running, Angry, VeryAngry, Sad, Swinging, Stabbing
	};

	public static enum Direction {
		West, East, North, South
	};

	public class TimedFrame {
		
		
		int frameTicks;
	}
}
