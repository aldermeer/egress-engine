package com.dysarcana.egressengine.components;

import com.artemis.Component;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class SpriteComponent extends Component {
	
	public String name;
	public Sprite sprite;

	public float r,g,b,a;
	
	public float scaleX, scaleY;
	public float rotation;
	
	public Layer layer = Layer.DEFAULT;
	
	
	public int getLayerId() {
        return layer.ordinal();
    }
	
	public SpriteComponent(Sprite spr) {
		sprite = spr;
		r = 1;
		g = 1;
		b = 1;
		a = 1;
	}

	
    public enum Layer {
        DEFAULT,
        BACKGROUND,
        ACTORS_1,
        ACTORS_2,
        ACTORS_3,
        PARTICLES;
        
    }

}
