package com.dysarcana.egressengine.components;

import com.artemis.Component;
import com.badlogic.gdx.utils.Array;

public class Interaction extends Component {
	
	Array<DialogOption> dialogOptions;
	Array<String> keyWord;
	
	public Interaction() {
		
	}

	public class DialogOption {
		
	}
	
	public Interaction addOption(DialogOption newOption) {
		
		dialogOptions.add(newOption);
		
		return this;
	}
}
