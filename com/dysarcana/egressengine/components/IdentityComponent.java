package com.dysarcana.egressengine.components;

import com.artemis.Component;
import com.artemis.Entity;

public class IdentityComponent extends Component {
	
	String editorID;
	String name;
	public boolean selectable; 
	
	//
	private Entity selectedEntity;
	
	/* Entity with whom this entity is having a conversation */
	private Entity conversationalEntity;


	public IdentityComponent(String newEditorID, String newName) {
		editorID = newEditorID;
		name =	   newName;
		
	}


	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
 
