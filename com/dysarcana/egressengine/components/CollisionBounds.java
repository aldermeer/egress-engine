package com.dysarcana.egressengine.components;


import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;

public class CollisionBounds extends Component {

	public float radius = 0.0f;
	public Vector2 offset = new Vector2();


	/**
	 * @param x Rightward offset from center
	 * @param y Upward offset from center
	 * @param radius
	 */
	public CollisionBounds(float x, float y, float radius) {
		offset.x = x;
		offset.y = y;
		this.radius = radius;
		
	}
	
}
