package com.dysarcana.egressengine;

import com.artemis.Component;
import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.dysarcana.egressengine.components.CircularMotion;
import com.dysarcana.egressengine.components.CollisionBounds;
import com.dysarcana.egressengine.components.DebugComponent;
import com.dysarcana.egressengine.components.FocalPoint;
import com.dysarcana.egressengine.components.IdentityComponent;
import com.dysarcana.egressengine.components.Location;
import com.dysarcana.egressengine.components.PlayerComponent;
import com.dysarcana.egressengine.components.SpriteComponent;
import com.dysarcana.egressengine.components.VelocityComponent;

public abstract class EntityFactory {
	static int lastID;
	

	public static Entity createPlayer(World world, String newEditorID, String newVisibleName, Texture textureAsset,
								int spriteTextureX, int spriteTextureY, int width, int height,
								float worldX, float worldY, float velocityX, float velocityY) {

	
		Entity e = createSentient(world, newEditorID, newVisibleName, worldX, worldY, velocityX, velocityY)
		.addComponent(new FocalPoint(worldX+300, worldY+200))
		.addComponent(new SpriteComponent(new Sprite(textureAsset,0,0,width, height)))

		//.addComponent(new Component())
		//.addComponent(new FactionComponent())
		//.addComponent(new Component())
		
		.addComponent(new PlayerComponent());
		
		return e;
	}

 
	public static Entity createCirclingEntity(World world, String newEditorID, String newVisibleName, float x, float y, float vx, float vy) {
		Entity e = createSentient(world, newEditorID, newVisibleName, x, y, vx, vy)
		.addComponent(new CircularMotion());
		
		
		return e;
	}
	
	public static Entity createSentient(World world, String newEditorID, String newVisibleName, float x, float y, float vx, float vy) {

		Entity e = createStaticCollidable(world, newEditorID, newVisibleName, x, y)
		.addComponent(new VelocityComponent(vx,vy));
		
		return e;
	}
	
	public static Entity createStaticCollidable(World world, String newEditorID, String newVisibleName, float x, float y) {
		Entity e = createEntity(world, newEditorID, newVisibleName);
		
		e.addComponent(new Location(x,y))
		.addComponent(new CollisionBounds(0f, 0f, 10f));
		
		return e;
		
	}
	
	private static Entity createEntity(World world, String newEditorID, String newVisibleName) {
		Entity e = world.createEntity();
		e.addComponent(new IdentityComponent(newEditorID,newVisibleName));

		return e;
	}



}

