package com.dysarcana.egressengine.systems;

//import javax.swing.text.Position;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;
import com.artemis.annotations.Mapper;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.dysarcana.egressengine.components.CollisionBounds;
import com.dysarcana.egressengine.components.Location;
import com.dysarcana.egressengine.components.SpriteComponent;

public class SpriteRenderSystem extends EntityProcessingSystem {

	@Mapper ComponentMapper<Location> lm;
	@Mapper ComponentMapper<SpriteComponent> sm;
	@Mapper ComponentMapper<CollisionBounds> cm;
	
	Vector2 offset = new Vector2(0,0);
	
    private SpriteBatch batch;
    private OrthographicCamera camera;
    
    public SpriteRenderSystem(OrthographicCamera camera, AssetManager newAssetManager) {
        super(Aspect.getAspectForAll(Location.class, SpriteComponent.class));
        this.camera = camera;
	}
    
	public void initialize(SpriteBatch batch) {
    	this.batch = batch;
    }
    
	private void process(Entity e) {
		if (lm.has(e)) {
			Location location = lm.getSafe(e);
			SpriteComponent sc = sm.getSafe(e);
			CollisionBounds cbc = cm.getSafe(e);
			
			batch.setColor(sc.r, sc.g, sc.b, sc.a);
			float locx;
			float locy;
			offset.set( (sc.sprite.getWidth()/2), cbc.radius );

			

			if (sc.sprite != null) {
				locx = location.vec.x - offset.x;
				locy = location.vec.y - offset.y;
				batch.draw(sc.sprite,locx,locy);

			} else System.out.println("Skipping sprite for " + e.toString());
			
			
		}
		
	}
	
	@Override
	protected void begin() {
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
	}
	
	@Override
	protected void processEntities(ImmutableBag<Entity> entities) {
		for (int i = 0; i < entities.size(); i++) {
			process(entities.get(i));
		}
		
	}

	@Override
	protected void end() {
		batch.end();
	}
	
	@Override
	protected boolean checkProcessing() {
		// TODO Auto-generated method stub
		return true;
	}

}
