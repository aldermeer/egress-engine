package com.dysarcana.egressengine.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;
import com.artemis.annotations.Mapper;
import com.artemis.utils.ImmutableBag;
import com.dysarcana.egressengine.components.AnimationComponent;
import com.dysarcana.egressengine.components.SpriteAnimation;
import com.dysarcana.egressengine.components.SpriteComponent;
import com.dysarcana.egressengine.components.VelocityComponent;

public class SpriteAnimationSystem extends EntityProcessingSystem {
	
	@Mapper ComponentMapper<SpriteComponent> sm;
	@Mapper ComponentMapper<VelocityComponent> vm;
	@Mapper ComponentMapper<AnimationComponent> am;

	
	public SpriteAnimationSystem() {
		super(Aspect.getAspectForAll(SpriteComponent.class, SpriteAnimation.class, VelocityComponent.class));
		// TODO Auto-generated constructor stub
	}

	private void process(Entity e) {
		
	}
	
	@Override
	protected void processEntities(ImmutableBag<Entity> entities) {
		for (int i = 0; i < entities.size(); i++) {
			process(entities.get(i));
		}
		
	}

	@Override
	protected boolean checkProcessing() {
		// TODO Auto-generated method stub
		return false;
	}
	

}
