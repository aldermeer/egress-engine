package com.dysarcana.egressengine.systems;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Mapper;
import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.dysarcana.egressengine.components.Location;
import com.dysarcana.egressengine.components.SpriteComponent;

public class UIRenderingSystem extends VoidEntitySystem implements InputProcessor{
	
	ShapeRenderer sr;
	
	@Mapper ComponentMapper<Location> pm;
    @Mapper ComponentMapper<SpriteComponent> sm;
    
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private BitmapFont font;

    // width of the entire screen
	private int screenWidth;
	
    // height of the entire screen
	private int screenHeight;
	
	// width of one resource bar
	private int barWidth;
	
	// height of the resource bars
	private int barHeight;

	//
	private int firstBarX;

	//
	private int barPadding;
	
	int ticksBeforeReset;
	
	Vector2 mousePos;
	
	
	
	public UIRenderingSystem(OrthographicCamera camera) {
		super();
		//this.camera = camera;
	}

	@Override
	protected void initialize() {

		ticksBeforeReset = 60;
/*        Texture fontTexture = new Texture(Gdx.files.internal("fonts/normal_0.png"));
        fontTexture.setFilter(TextureFilter.Linear, TextureFilter.MipMapLinearLinear);
        TextureRegion fontRegion = new TextureRegion(fontTexture);
        font = new BitmapFont(Gdx.files.internal("fonts/normal.fnt"), fontRegion, false);+
        
        font.setUseIntegerPositions(false);				*/
        sr = new ShapeRenderer();
		// TODO Auto-generated method stub
        
		screenWidth = (int)Gdx.graphics.getWidth();
		screenHeight = (int)Gdx.graphics.getHeight();
		
		barWidth = screenWidth/4;
		barHeight = screenHeight/100;
		
		barPadding = screenHeight/100;
		

	}

	@Override
	protected void begin() {

	}

	@Override
	protected void end() {

	}

	@Override
	protected void processSystem() {
		
		ticksBeforeReset--;
		if (ticksBeforeReset < 1) {
			ticksBeforeReset = 60;
			
			screenWidth = (int)Gdx.graphics.getWidth();
			screenHeight = (int)Gdx.graphics.getHeight();
			
			barWidth = screenWidth/4;
			barHeight = screenHeight/100;
			
			barPadding = screenHeight/100;
			
			 
			    int x, y;
				x = Gdx.input.getX();
				y = Gdx.input.getY();
				
			
		}
			
		
		sr.setColor(Color.RED);
		sr.begin(ShapeType.Filled);
		sr.rect(firstBarX, barPadding, barWidth-20, barHeight);
		sr.end();
		
		sr.setColor(Color.BLACK);
		sr.begin(ShapeType.Line);
		sr.rect(firstBarX, barPadding, barWidth, barHeight);
		sr.end();

		
		
		sr.setColor(Color.GREEN);
		sr.begin(ShapeType.Filled);
		sr.rect(firstBarX + barWidth + barPadding*2, barPadding, barWidth-20, barHeight);
		sr.end();
		
		sr.setColor(Color.BLACK);
		sr.begin(ShapeType.Line);
		sr.rect(firstBarX + barWidth + barPadding*2, barPadding, barWidth, barHeight);
		sr.end();

		
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
