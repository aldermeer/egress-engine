package com.dysarcana.egressengine.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.dysarcana.egressengine.components.Expires;
 
public class ExpiringSystem extends EntityProcessingSystem {
	@Mapper
	ComponentMapper<Expires> em;

	public ExpiringSystem() {
		super(Aspect.getAspectForAll(Expires.class));
	}

	@Override
	protected boolean checkProcessing() {
		return true;
	}

	@Override
	protected void process(Entity e) {
		Expires expires = em.get(e);
		expires.delay -= world.getDelta(); 
		if (expires.delay <= 0) {
			e.deleteFromWorld();
		}
	}
}
