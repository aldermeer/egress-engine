package com.dysarcana.egressengine.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.dysarcana.egressengine.components.CollisionBounds;
import com.dysarcana.egressengine.components.DebugComponent;
import com.dysarcana.egressengine.components.FocalPoint;
import com.dysarcana.egressengine.components.IdentityComponent;
import com.dysarcana.egressengine.components.Location;
import com.dysarcana.egressengine.components.PlayerComponent;
import com.dysarcana.egressengine.components.VelocityComponent;

public class DebugSystem extends EntityProcessingSystem {
	
	@Mapper ComponentMapper<Location> lm;
	@Mapper ComponentMapper<DebugComponent> dbm;
	@Mapper ComponentMapper<CollisionBounds> bm;
	@Mapper ComponentMapper<FocalPoint> fm;
	@Mapper ComponentMapper<VelocityComponent> vm;
	@Mapper ComponentMapper<PlayerComponent> pm;
	@Mapper ComponentMapper<IdentityComponent> im;
	
	
	FocalPoint foc;
	VelocityComponent vel;
	Location playerloc;
	Vector2 focalLoc; 

	private SpriteBatch batch;
	private BitmapFont font;
	private Camera camera;
	String name;
	
	Vector2 loc2;
	Vector3 loc3;

	Vector3 nameOffset = new Vector3();

	Vector3 nameOnScreen = new Vector3(0f, 0f, 0f);

	ShapeRenderer sr;

	int wait = 0;
	
	public DebugSystem(Camera camera, BitmapFont font) {
		super(Aspect.getAspectForAll(
				DebugComponent.class
				));
		
		Gdx.app.debug("","Debugger added - " + this.toString());
		
		sr = new ShapeRenderer();
		this.camera = camera;
		sr.setProjectionMatrix(camera.combined);
		focalLoc = new Vector2();
		this.font = new BitmapFont();
		batch = new SpriteBatch();
		loc3 = new Vector3();
		
	
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void process(Entity e) {
		camera.update();
		loc2 = lm.get(e).vec;
		loc3.set(loc2.x, loc2.y, 0);
		CollisionBounds bound = bm.get(e);
		
		// get focal for player Entity
		if (fm.has(e)) {
			Vector2 foc = fm.get(e).vec;
		}


		//handling of debug component
		if (dbm.has(e) && bm.has(e)) {
			
			bound = bm.get(e);
			playerloc = lm.getSafe(e);
			
			// set color of bounding radius
	
			// static
			Color boundingColor=Color.BLACK;
	
			// velocity
			if (vm.has(e))
				boundingColor = Color.RED;
	
			// player
			if (pm.has(e))
				boundingColor = Color.BLUE;
			
			boundingColor.a = 200;
			sr.setColor(boundingColor);
			
	
		// Render bounding boxes in proper color
			Vector2 boundingCircleLoc = new Vector2(
				loc3.x + bound.offset.x - camera.position.x + camera.viewportWidth/2,
				loc3.y + bound.offset.y - camera.position.y + camera.viewportHeight/2);
		

			sr.begin(ShapeType.Line);
				sr.scale(1, 1, 1);
				sr.circle(boundingCircleLoc.x, boundingCircleLoc.y,
						  bound.radius);
				//Gdx.app.debug("", "Rendering " + e.toString());
			sr.end();
			
			if (im.has(e) && im.get(e).getName() != null) {

				// Draw character names on screen
				batch.begin();
				
					name = im.get(e).getName();
					
					nameOffset.x = 0-font.getBounds(name).width/2;
					
					// the height of a unit is -very roughly- eight times the bounding circle radius.
					nameOffset.y = bound.radius*8;
					
					nameOnScreen.x = loc3.x + nameOffset.x - camera.position.x + camera.viewportWidth/2;
					nameOnScreen.y = loc3.y + nameOffset.y - camera.position.y + camera.viewportHeight/2; 

					
					font.draw(batch, name,
						nameOnScreen.x,
						nameOnScreen.y);
					
				batch.end();
				
			}

			
		}
		
		if (fm.has(e)) {

			foc = fm.getSafe(e);
			vel = vm.getSafe(e);
			
			focalLoc.x = (foc.vec.x - camera.position.x + camera.viewportWidth/2);
			focalLoc.y = (foc.vec.y - camera.position.y + camera.viewportHeight/2);
			
			sr.begin(ShapeType.Line);
				sr.setColor(Color.GRAY);
				sr.scale(1, 1, 1);
	
				sr.circle(focalLoc.x, focalLoc.y, 6);
				sr.circle(focalLoc.x, focalLoc.y, 10);
			sr.end();

				if (wait == 300) {
					debugOutput();
					wait = 0;
				}
				wait++;

			
		}
		camera.update();
				
		// TODO
			// Render HP
			// Render Mana
		
	}

	private void debugOutput() {
		Gdx.app.setLogLevel(Gdx.app.LOG_DEBUG);
		Gdx.app.debug("","Focal: (" + foc.vec.x + ", " + foc.vec.y + ")");
		Gdx.app.debug("","Motion: (" + vel.vec.x + ", " + vel.vec.y + ")");
		Gdx.app.debug("","Location: (" + playerloc.vec.x + ", " + playerloc.vec.y + ")");
		Gdx.app.debug("","Camera: (" + camera.position.x + ", " + camera.position.y + ")");
		Gdx.app.debug("","Name offset: (" + nameOffset.x + ", " + camera.position.y + ")");
		
	}
	
	
}
