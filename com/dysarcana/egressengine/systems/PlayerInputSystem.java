package com.dysarcana.egressengine.systems;

import java.io.Console;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;
import com.artemis.annotations.Mapper;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.dysarcana.egressengine.EntityFactory;
import com.dysarcana.egressengine.components.FocalPoint;
import com.dysarcana.egressengine.components.Location;
import com.dysarcana.egressengine.components.PlayerComponent;
import com.dysarcana.egressengine.components.VelocityComponent;
import com.dysarcana.egressengine.components.CollisionBounds;
import com.badlogic.gdx.scenes.scene2d.Actor;
/**
 * handles input for the player entity 
 * @author GRECO
 *
 */
public class PlayerInputSystem extends EntityProcessingSystem implements InputProcessor {

	// handles and distributes inputs 
	InputMultiplexer inputMultiplexer = new InputMultiplexer(this);
	
	
	@Mapper ComponentMapper<FocalPoint> focalMapper;
	@Mapper ComponentMapper<VelocityComponent> velocityMapper;
	@Mapper ComponentMapper<Location> locationMapper;
	@Mapper ComponentMapper<CollisionBounds> bm;
	
	
	// Controls 
	private boolean up, down, left, right, camSouth, camWest, camEast, camNorth, reset, leftClick, rightClick;
	
	private OrthographicCamera camera;
	private Vector3 mouseVector;
	
	VelocityComponent velocity;
	Vector3 focalPoint;
	Vector2 aimVec; 
	Location loc;
	Entity selectedEntity;
	int x,y;
	
	String motiveLine = "Directives:";
	
	boolean refocus = false;
	boolean radialMode = false;
	
	int motiveWait = 0; // TODO Currently

	
	public PlayerInputSystem(OrthographicCamera camera) {
		super(Aspect.getAspectForAll(VelocityComponent.class, PlayerComponent.class, FocalPoint.class, Location.class));
		this.camera = camera;
	}
	
	@Override
	protected void initialize() {
		Gdx.input.setInputProcessor(inputMultiplexer);
		focalPoint = new Vector3(0,0,0);
		//inputMultiplexer.addProcessor();

	}

	protected void process(Entity e) {
		// update camera (needed for unprojection)
		camera.update();
				
		// grab focal (relative to camera?)
		focalMapper.get(e).vec.set(focalPoint.x, focalPoint.y);

		// map velocity and location of entity to local identifiers
		velocity = velocityMapper.get(e);
		loc = locationMapper.get(e);
		
	
		// create the vector which will determine how far the player can move in one tick.
		aimVec = new Vector2(
			focalPoint.x - loc.vec.x,
			focalPoint.y - loc.vec.y);
		
		
		// Reset velocity to prepare for new velocity control determination 
		// Whether this stays this simple is a design decision.
			// 
			// Player character will eventually be able to be moved by other forces
		
		velocity.vec.set(0,0);
		if (radialMode) {
			if(left) {
				velocity.vec.add(aimVec.cpy().rotate(90)); 
			} 
	
			if(right) {
				velocity.vec.add(aimVec.cpy().rotate(-90));
			}
	
			if(up) {
				velocity.vec.add(aimVec.cpy());
			}
			
			if(down) {
				velocity.vec.add(aimVec.cpy().rotate(180f));
			}
		
		} else {
	
			if(left) {
				velocity.vec.x = -1; 
			} 
	
			if(right) {
				velocity.vec.x = 1;
			}
	
			if(up) {
				velocity.vec.y = 1;
			}
			
			if(down) {
				velocity.vec.y = -1;
			}
			
		}
		
		velocity.vec.nor().mul(3);


		// TODO: Debugging
		motiveLine = "";
		
		if (motiveWait == 300) {
			motiveCap();
			Gdx.app.log("","Movement: " + velocity.vec.x + "," + velocity.vec.y + " - " + velocity.vec.toString() + "\n\r");
			Gdx.app.log("","Velocity: " + velocity.vec.len() );

			motiveWait = 0;

		}
		motiveWait++;

	
		if(refocus) {
			x = Gdx.input.getX();
			y = Gdx.input.getY();
			
			focalPoint.x = x;
			focalPoint.y = y;
			camera.unproject(focalPoint);
			
			//Gdx.app.log("","Refocusing at " + focalPoint.x + "," + focalPoint.y);
		}
		
		if(reset) {
			velocity.vec.set(0, 0);
			loc.vec.set(20, 20);
		}
		if(camNorth) camera.position.y += 5;
		if(camSouth) camera.position.y -= 5;
		
		if(camEast) camera.position.x += 5;
		if(camWest) camera.position.x -= 5;
		
		// TODO make more betterized
		//if(bm.getSafe(e).radius > bm. mouseVector)

		
	}

	private void motiveCap() {
		
		motiveLine = "";
		
		if(left)
			motiveLine += "left ";

		if(right)
			motiveLine += "right ";

		if(up)
			motiveLine += "up ";
		
		if(down)
			motiveLine += "down";

		if(motiveLine.length() > 4)
			Gdx.app.log("",motiveLine);
		
		
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Input.Keys.A) {
			left = true;
		}
		else if(keycode == Input.Keys.W) {
			up = true;
		}
		else if(keycode == Input.Keys.S) {
			down = true;
		}
		else if(keycode == Input.Keys.D) {
			right = true;
		}
		else if(keycode == Input.Keys.TAB) {
			radialMode = !radialMode;
		}

		else if(keycode == Input.Keys.NUM_0)
			reset = true;

		else if(keycode == Input.Keys.NUM_2)
			camSouth = true;
		
		else if(keycode == Input.Keys.NUM_4)
			camWest = true;
		
		else if(keycode == Input.Keys.NUM_6)
			camEast = true;
		
		else if(keycode == Input.Keys.NUM_8)
			camNorth = true;

		return true;
	}
	
	// Keys separated from logic to provide for re-binding key controls

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Input.Keys.A)
			left = false;

		else if(keycode == Input.Keys.D)
			right = false;

		else if(keycode == Input.Keys.W)
			up = false;
		
		else if(keycode == Input.Keys.S)
			down = false;
		
		else if(keycode == Input.Keys.NUM_0)
			reset = false;
		
		else if(keycode == Input.Keys.NUM_0)
			reset = false;

		else if(keycode == Input.Keys.NUM_2)
			camSouth = false;
		
		else if(keycode == Input.Keys.NUM_4)
			camWest = false;
		
		else if(keycode == Input.Keys.NUM_6)
			camEast = false;
		
		else if(keycode == Input.Keys.NUM_8)
			camNorth = false;


		
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		if (button == Input.Buttons.RIGHT) {
			
			
			
			refocus = true;
		}
		//if (button == Input.Buttons.LEFT)
			// TODO create a damage area based on a character's weapon
			//aimVec.mul(5);
			//EntityFactory.createDamageArea(world,loc.vec.x+aimVec.x,loc.vec.y+aimVec.y,,2).addToWorld();

		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		if(button == Input.Buttons.RIGHT) {
			refocus = false;
		}
		return true;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	protected void processEntities(ImmutableBag<Entity> entities) {
		for (int i = 0; i < entities.size(); i++) {
			process(entities.get(i));
		}
	}

	@Override
	protected boolean checkProcessing() {
		// TODO Auto-generated method stub
		
		return true;
	}

}
