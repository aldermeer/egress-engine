package com.dysarcana.egressengine.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;

import com.dysarcana.egressengine.components.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.*;

public class MovementSystem extends EntityProcessingSystem {

	@Mapper ComponentMapper<Location> locationMapper;
	@Mapper ComponentMapper<VelocityComponent> velMapper;
	@Mapper ComponentMapper<CircularMotion> motMapper;
	private String debugLine = "";
	private int debugWait = 0;
	
	@SuppressWarnings("unchecked")
	public MovementSystem() {
		super(Aspect.getAspectForAll(Location.class,VelocityComponent.class));
	}
	
	/**
	 * Processes a single entity
	 */
	protected void process(Entity e) {
		// Get the components from the entity using the component mappers.
		Vector2 location = locationMapper.get(e).vec;
		Vector2 velocity = velMapper.get(e).vec;
		
		if(motMapper.get(e) instanceof CircularMotion ) { 
			velocity.rotate(10f);
			
			if (velocity.len() == 0)
				velocity.set(1f,1f);
		}
		
		// Update the position.
		location.add(velocity.cpy().mul(2));
	
		// debuggering
		{
			if (debugWait == 300) {
				debugLine = "";
				Gdx.app.debug("",debugLine);
				debugWait = 0;
			}
			debugWait++;
		}
		
		
		
	}

	@Override
	protected boolean checkProcessing() {
		// True if should be checked
		return true;
	}
}
