package com.dysarcana.egressengine.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.dysarcana.egressengine.components.Heading;
import com.dysarcana.egressengine.components.Location;
import com.dysarcana.egressengine.components.additional.DialogOptions;

public class DialogSystem extends EntityProcessingSystem {
	public DialogSystem() {
		super(Aspect.getAspectForAll(DialogOptions.class));
	}
	
	public void process(Entity e) {
		
	}
}
