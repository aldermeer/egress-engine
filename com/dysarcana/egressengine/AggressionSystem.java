package com.dysarcana.egressengine;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import com.dysarcana.egressengine.components.FocalPoint;
import com.dysarcana.egressengine.components.Location;
import com.dysarcana.egressengine.components.PlayerComponent;
import com.dysarcana.egressengine.components.VelocityComponent;

public class AggressionSystem extends EntityProcessingSystem {

	public AggressionSystem() {
		super(Aspect.getAspectForAll(VelocityComponent.class, PlayerComponent.class, FocalPoint.class, Location.class));
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void processEntities(ImmutableBag<Entity> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected boolean checkProcessing() {
		// TODO Auto-generated method stub
		return false;
	}

}
