package com.dysarcana.egressengine;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.InputMultiplexer;
import com.dysarcana.egressengine.screens.*;

/**
 * Buhhh
 */
public class eGress extends Game {

	InputMultiplexer inputMultiplexer;
	
		private GameplayScreen gameplayScreen;
		private eGressLogo logoScreen;
		private Splash splashScreen;
		private MainMenu mainMenu;

		
		// simple screen recorder
		// libx264
		// 

	
	@Override
	public void create() {
		inputMultiplexer = new InputMultiplexer();
		setScreen(getGameplay());
	}
	
	public GameplayScreen getGameplay()
    {
		if (gameplayScreen == null)
			gameplayScreen = new GameplayScreen(this);
			inputMultiplexer.addProcessor(gameplayScreen);
        return gameplayScreen;
    }
	
	public eGressLogo getLogo()
    {
		if (logoScreen == null)
		logoScreen = new eGressLogo( this );
		inputMultiplexer.addProcessor(logoScreen);
        return logoScreen; 
    }
	
	public Splash getSplash()
    {
		if (splashScreen == null)
        splashScreen = new Splash( this );
		inputMultiplexer.addProcessor(splashScreen);
        return splashScreen;
    }
	
	public MainMenu getMainMenu()
    {
		if (mainMenu == null)
		mainMenu = new MainMenu( this );
		inputMultiplexer.addProcessor(mainMenu);
        return mainMenu; 
    }
	


}
