package com.dysarcana.egressengine.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.dysarcana.egressengine.eGress;

/**
 * Abstract Screen
 * @author GRECO
 *
 */
public abstract class AbstractScreen implements Screen {
	
	public static final int GAME_VIEWPORT_WIDTH = 400, GAME_VIEWPORT_HEIGHT = 240;
    public static final int MENU_VIEWPORT_WIDTH = 800, MENU_VIEWPORT_HEIGHT = 480;
    
	public static boolean DEBUG_MODE = true;
	
    protected final eGress egress;
    
	protected final Stage stage;

	private BitmapFont font;
	private SpriteBatch batch;
	private Skin skin;
	private TextureAtlas atlas;
	private Table table;
	
	
	public AbstractScreen(eGress egress) {
		this.egress	= egress;
		this.stage 	= new Stage(0,0,true);
		
		// TODO Auto-generated constructor stub
	}
	
	protected String getName()
    {
        return getClass().getSimpleName();
    }

    protected boolean isGameScreen()
    {
        return false;
    }

	//Lazy loaded
	public BitmapFont getFont()
    {
        if( font == null ) {
            font = new BitmapFont();
        }
        return font;
    }

    public SpriteBatch getBatch()
    {
        if( batch == null ) {
            batch = new SpriteBatch();
        }
        return batch;
    }
    
    public TextureAtlas getAtlas()
    {
        if( atlas == null ) {
            atlas = new TextureAtlas( Gdx.files.internal( "image-atlases/pages.atlas" ) );
        }
        return atlas;
    }

    protected Skin getSkin()
    {
        if( skin == null ) {
            FileHandle skinFile = Gdx.files.internal( "skin/uiskin.json" );
            skin = new Skin( skinFile );
        }
        return skin;
    }

    protected Table getTable()
    {
        if( table == null ) {
            table = new Table( getSkin() );
            table.setFillParent( true );
            if( DEBUG_MODE ) {
                table.debug();
            }
            stage.addActor( table );
        }
        return table;
    }

    
    public void render(float delta) {
	    {
	        // the following code clears the screen with the given RGB color (black)
	        Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
	        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
	    }
	}
	
	@Override
	public void show() {
		Gdx.app.log( "", "Showing screen: " + getName() );
		
	}
		
	@Override
	public void hide() {
		Gdx.app.log( "", "Hiding screen: " + getName() );
		
	}

	@Override
	public void pause() {
		Gdx.app.log( "", "Pausing screen: " + getName() );
		
	}

	@Override
	public void resume() {
		Gdx.app.log( "", "Resuming screen: " + getName() );
		
	}
	
	@Override
	public void resize(int width, int height) {
		Gdx.app.log( "", "Resizing screen: " + getName()
				               + " to: " + width + " x " + height );

        // resize the stage
        stage.setViewport( width, height, true );
	}
	

}
