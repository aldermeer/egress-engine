package com.dysarcana.egressengine.screens;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dysarcana.egressengine.eGress;

public class Splash extends AbstractScreen implements InputProcessor {
	
	private Texture splashTexture;
    private TextureRegion splashTextureRegion;

	public Splash(eGress eGress) {
		super(eGress);
		
	}

	@Override
	public void show() {
		
		super.show();
		 
        // load the splash image and create the texture region
        splashTexture = new Texture("data/libgdx.png" );
 
        // we set the linear texture filter to improve the stretching
        splashTexture.setFilter( TextureFilter.Linear, TextureFilter.Linear );
 
        // in the image atlas, our splash image begins at (0,0) at the
        // upper-left corner and has a dimension of 512x301
        splashTextureRegion = new TextureRegion( splashTexture, 0, 0, 512, 301 );
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
