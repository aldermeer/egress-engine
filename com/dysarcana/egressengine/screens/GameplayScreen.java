package com.dysarcana.egressengine.screens;

import java.util.Random;

import com.artemis.ComponentManager;
import com.artemis.Entity;
import com.artemis.EntityManager;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.dysarcana.egressengine.AggressionSystem;
import com.dysarcana.egressengine.EntityFactory;
import com.dysarcana.egressengine.eGress;
import com.dysarcana.egressengine.components.DebugComponent;
import com.dysarcana.egressengine.components.IdentityComponent;
import com.dysarcana.egressengine.systems.DebugSystem;
import com.dysarcana.egressengine.systems.ExpiringSystem;
import com.dysarcana.egressengine.systems.MovementSystem;
import com.dysarcana.egressengine.systems.PlayerInputSystem;
import com.dysarcana.egressengine.systems.SpriteAnimationSystem;
import com.dysarcana.egressengine.systems.SpriteRenderSystem;
import com.dysarcana.egressengine.systems.UIRenderingSystem;

public class GameplayScreen extends AbstractScreen implements InputProcessor {


	public static final String LOG_FILENAME = "data/eGressLog.log";

	/* Managers */
	static private AssetManager Assets;
	private com.artemis.World artemisWorld;
	private com.badlogic.gdx.physics.box2d.World box2dworld;
	
	private SpriteRenderSystem spriteRenderSystem;
	private UIRenderingSystem uiRenderingSystem;
	private DebugSystem debugSystem;
	private PlayerInputSystem playerInputSystem;
	private MovementSystem movementSystem;
	private ExpiringSystem expiringSystem;
	private SpriteAnimationSystem spriteAnimationSystem;
	private AggressionSystem aggressionSystem;
	

	private OrthographicCamera camera;
	private SpriteBatch spriteBatch;
	private FPSLogger fpsLogger;

	private ShapeRenderer sr;
	private IdentityComponent id;

	
	int renders;
	float graphicsWidth, graphicsHeight;
	
	Texture gdxLogoTex, claudiusTex;
	
	Entity biggs, wedge, player, mobile;
	
	BitmapFont font;
	

//	Pool<HeightLevel> levels;
	
	public GameplayScreen(eGress eGress) {
		super(eGress);
		
		graphicsWidth = Gdx.graphics.getWidth();
		graphicsHeight = Gdx.graphics.getHeight();

		camera = new OrthographicCamera(1600, 900);
		camera.setToOrtho(false, graphicsWidth, graphicsHeight);
		
		Assets = new AssetManager();
		
		spriteBatch = new SpriteBatch();
		sr = new ShapeRenderer();
		
		initArtemisWorld();
		
		loadAssets();

		//initBox2dWorld();
		addTestEntities();
		
		fpsLogger = new FPSLogger();
		
		renders = 0;
		font = new BitmapFont();
		

	}

	//String base = "";
	String base = "";
	String gdxLogo = base + "assets/data/libgdx.png";
	String claudius = base + "assets/data/sprites_map_claudius.png";

	private void loadAssets() {
		/*
 		//Texture.setAssetManager(Assets);
		
 		Assets.load(gdxLogo, Texture.class);
 		Assets.load(claudius, Texture.class);
 		
 		if (!Assets.isLoaded(gdxLogo, Texture.class))
 			//debuggery
 		System.out.println("Asset not loaded: " + gdxLogo + " of type " + Assets.getAssetType(gdxLogo));
 		else
	    gdxLogoTex = Assets.get(gdxLogo, Texture.class);
		
 		if (!Assets.isLoaded(claudius, Texture.class))
 			//debuggery
 		System.out.println("Asset not loaded: " + claudius + " of type " + Assets.getAssetType(claudius));
 		else
	    claudiusTex = Assets.get(claudius, Texture.class);
 		
 		Assets.finishLoading();*/


	}

	private void addTestEntities() {
		
		Gdx.app.log("","addTestEntities()");
		
		//claudiusTex = Assets.get(claudius);
		
		claudiusTex = new Texture(Gdx.files.internal(claudius));

		player  = EntityFactory.createPlayer(this.artemisWorld, "player", "Not My Claudius", claudiusTex, 1, 1, 32, 63, 100, 100, 0, 0);
		player.addComponent(new DebugComponent())
		.addToWorld();
		
		Gdx.app.log("", "Player: " + player.toString() + " added to world.\r");
		
		biggs = EntityFactory.createStaticCollidable(this.artemisWorld, "biggs", "Biggs", 500, 200);
		biggs.addToWorld();

		biggs.addComponent(new DebugComponent());
		Gdx.app.log("","Static: " + biggs.toString() + " added to world.");

		wedge = EntityFactory.createCirclingEntity(this.artemisWorld, "wedge", "Wedge", 200, 300, 1, 1);
		wedge.addToWorld();

		biggs.addComponent(new DebugComponent());
		Gdx.app.log("","Static: " + wedge.toString() + " added to world.");

		// number of random entities to add
		int numRands = 20;
		int randX;
		int randY;
		Entity randEnt;

		Random randGen = new Random();
		
		for(int i = 0;i < numRands;i++) {
			
			randX = randGen.nextInt(700) + 100;
			randY = randGen.nextInt(500) + 100;
			
			if (i < 10)
				randX += 300;
			
			randEnt = EntityFactory.createStaticCollidable(this.artemisWorld,"random"+i,"Random #"+i, randX, randY);
			randEnt.addToWorld();
			
			id = ((IdentityComponent)randEnt.getComponent(IdentityComponent.class));
			switch(i % 5) {
			case 1:
				id.setName(null);
				break;
			case 2:
				id.setName("Wing Guard Archer");
				break;
			case 3:
				id.setName("Wing Guard Trooper");
				break;
			case 4:
				id.setName("#"+i);
				break;
			default:
				
			}

			
			randEnt.addComponent(new DebugComponent());
			Gdx.app.log("","Static: " + randEnt.toString() + " added to world at (" + randX + "," + randY + ")" );
			
		}
		
		
		mobile = EntityFactory.createCirclingEntity(this.artemisWorld, "circly","Circly", 50, 50, 1, 1);
		mobile.addToWorld();
		Gdx.app.log("","Circly: " + mobile.toString() + " added to world.");
		
	}
		

	
	
	private String isActiveString(Entity ent) {
		if (ent.isActive()) {
			return ent.toString() + ": active\n\r";
		} 
		else return ent.toString() + " inactive\n\r";
	}

	private void initArtemisWorld() {
		Gdx.app.debug("","initArtemisWorld()");
		artemisWorld = new com.artemis.World();

		artemisWorld.setManager(new EntityManager());
		artemisWorld.setManager(new ComponentManager());
		artemisWorld.setManager(new TagManager());

		playerInputSystem 		= artemisWorld.setSystem(new PlayerInputSystem(camera));
		movementSystem 			= artemisWorld.setSystem(new MovementSystem());
		expiringSystem 			= artemisWorld.setSystem(new ExpiringSystem());
		spriteAnimationSystem	= artemisWorld.setSystem(new SpriteAnimationSystem());
		
		spriteRenderSystem	= artemisWorld.setSystem(new SpriteRenderSystem(camera, Assets));
		
		debugSystem 		= artemisWorld.setSystem(new DebugSystem(camera, font));
		
		uiRenderingSystem	= artemisWorld.setSystem(new UIRenderingSystem(camera));
		
		//artemisWorld.setSystem();

		artemisWorld.initialize();
		
		spriteRenderSystem.initialize(spriteBatch);
	
	}


	@Override
	public void render(float delta) {
		super.render(delta);
		//updateDebugString();
		
        // output the current FPS
        //fpsLogger.log();
        
		Gdx.gl.glClearColor(0.25f, 1.0f, 1.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		/**/
		spriteBatch.setProjectionMatrix(camera.combined);

		renders++;
		update();
		
	}
	
	public void update() {
		
		artemisWorld.process();
		
	}
	
	int updates = 0;
	public void updateDebugString() {
		updates++;
		if(updates == 300) {
			String actives = "\n";
			actives += isActiveString(player);
			actives += isActiveString(biggs);
			actives += isActiveString(wedge);
			actives += isActiveString(mobile);
	
			Gdx.app.log("",actives);
			updates = 0;
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	

/*
	@Override
	public void dispose() {
		try {
			assets.unload("data/droplet.png");
			assets.unload("data/libgdx.png");
			assets.unload("data/bucket.png");
			
			assets.unload("data/drop.wav");
		    assets.unload("data/rain.mp3");
		    
		    assets.clear();
			
		} catch(Exception ex) { }
		
	}
	*/

}
