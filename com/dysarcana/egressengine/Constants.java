package com.dysarcana.egressengine;

public abstract class Constants {
        
    public static abstract class Groups {
   	
    	public static final String PLAYER_BOUND 		= "player ship";
        public static final String PLAYER_PROJECTILES 	= "player bullets";

        public static final String FRIENDLY_SHIP 	= "player ship";
        public static final String FRIENDLY_BULLETS = "player bullets";

        public static final String ENEMY_SHIPS 		= "enemy ships";
        public static final String ENEMY_BULLETS 	= "enemy bullets";
    }

}
