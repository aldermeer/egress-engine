package com.dysarcana;

import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;

public class ImagePacker {

	public static void run() {
		Settings settings = new Settings();

		TexturePacker2.process(settings.outputFormat, "textures-original", "resources/textures");
	}

}
